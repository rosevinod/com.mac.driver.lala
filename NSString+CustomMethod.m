//
//  NSString+CustomMethod.m
//  Driver Lala
//
//  Created by Apple on 29/04/15.
//  Copyright (c) 2015 DriverLala. All rights reserved.
//

#import "NSString+CustomMethod.h"

@implementation NSString (CustomMethod)

+ (NSString*)MD5:(NSString *)string {
    
    // Create pointer to the string as UTF8
    const char *ptr = [string UTF8String];
    
    // Create byte array of unsigned chars
    unsigned char md5Buffer[CC_MD5_DIGEST_LENGTH];
    
    // Create 16 byte MD5 hash value, store in buffer
    CC_MD5(ptr, strlen(ptr), md5Buffer);
    
    // Convert MD5 value in the buffer to NSString of hex values
    NSMutableString *output = [NSMutableString stringWithCapacity:CC_MD5_DIGEST_LENGTH * 2];
    for(int i = 0; i < CC_MD5_DIGEST_LENGTH; i++)
        [output appendFormat:@"%02x",md5Buffer[i]];
    
    return output;
}


@end