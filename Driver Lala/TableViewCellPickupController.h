//
//  TableViewCellPickupController.h
//  Driver Lala
//
//  Created by Apple on 16/04/15.
//  Copyright (c) 2015 DriverLala. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TableViewCellPickupController : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *orderNameLabel;

@property (weak, nonatomic) IBOutlet UILabel *orderPlaceLabel;


@end
