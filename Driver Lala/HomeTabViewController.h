//
//  HomeTabViewController.h
//  Driver Lala
//
//  Created by Apple on 29/04/15.
//  Copyright (c) 2015 DriverLala. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomProtocol.h"

@interface HomeTabViewController : UITabBarController < CustomDelegate >

@property (nonatomic,retain) CustomProtocol *objCustomProtocol;

@end
