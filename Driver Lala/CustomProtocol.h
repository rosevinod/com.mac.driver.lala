//
//  CustomProtocol.h

#import <UIKit/UIKit.h>
#import "Reachability.h"

@protocol CustomDelegate <NSObject>

// These are the method can be defined in any other class
@optional

- (void) processCompleted;
- (void) authenticationResults:(NSData *) authResultData;

@end

@interface CustomProtocol : NSObject {
    
    // Delegate to respond back, This variable all you to access method of both classes
    id <CustomDelegate> _delegate;
}

@property (nonatomic,strong) id delegate;

- (NSString *) checkAuthentication:(NSString * ) username : (NSString *) password;

- (NSData *) JSONHandler:(NSString *) siteURL;

- (void) setUserDefaults:(NSString *) setObject key:(NSString *) forKey;

- (NSString *) getUserDefaults:(NSString *) objectForKey;

- (NSString *) getPlistVars:(NSString *) keyName;

// Check is network available in iPhone/iPad
- (NetworkStatus) checkNetworkAvailability;

- (void) showAlertMessage:(NSString *) message;

- (void) activityIndicator:(UIView *)view container:(UIView *)container loadingView:(UIView *)loadingView actInd:(UIActivityIndicatorView *)actInd status:(BOOL)status;

@end
