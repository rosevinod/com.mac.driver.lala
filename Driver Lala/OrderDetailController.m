//
//  OrderDetailController.m
//  Driver Lala
//
//  Created by Apple on 17/04/15.
//  Copyright (c) 2015 DriverLala. All rights reserved.
//

#import "OrderDetailController.h"

@interface OrderDetailController ()
    

@end

@implementation OrderDetailController

@synthesize orderNameLabel, orderName;

// pgrm mark ----- -------

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    NSLog(@"viewDidLoad %@",NSStringFromClass([self class]));
    
    self.orderNameLabel.text = orderName;
    
}

// pgrm mark ----- -------

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:true];
    
    NSLog(@"viewWillAppear %@",NSStringFromClass([self class]));
    
}

// pgrm mark ----- -------

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:true];
    NSLog(@"viewDidAppear %@",NSStringFromClass([self class]));
    
}


// pgrm mark ----- -------

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
