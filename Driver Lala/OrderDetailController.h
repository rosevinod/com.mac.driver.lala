//
//  OrderDetailController.h
//  Driver Lala
//
//  Created by Apple on 17/04/15.
//  Copyright (c) 2015 DriverLala. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface OrderDetailController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *orderNameLabel;

@property (weak, nonatomic) NSString *orderName;

@end
