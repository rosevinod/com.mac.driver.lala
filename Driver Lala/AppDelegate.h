//
//  AppDelegate.h
//  Driver Lala
//
//  Created by Apple on 01/04/15.
//  Copyright (c) 2015 DriverLala. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

