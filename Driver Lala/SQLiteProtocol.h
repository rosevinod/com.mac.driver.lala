//
//  SQLiteProtocol.h
//  Spine MD
//
//  Created by arroWebs Inc on 19/10/13.
//  Copyright (c) 2013 arroWebs Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <sqlite3.h>

@protocol SQLiteProtocolDelegate <NSObject>

@optional
@end

@interface SQLiteProtocol : NSObject {

    id <SQLiteProtocolDelegate> _delegate;
}
// Delegate parameter
@property (nonatomic,strong) id delegate;

// SQLite parameter
@property (strong, nonatomic) NSString *databasePath;
@property (nonatomic) sqlite3 *myDatabase;
@property (strong, nonatomic) NSString *statusOfAddingToDB;
@property (strong, retain) NSMutableArray *searchResult;

// SQLite Methods
- (void) prepareJSONDatabase;

// Save API call value and its related json value into database
- (void) saveToDatabase:(NSString *) apiCallName : (NSString *) jsonFileName : (NSData *) jsonData;

// Save different value into database
- (void) saveImagesToDatabase:(NSString *) apiCallName fileName:(NSString *) fileName;

// Get the value of json, will send an api call for which we get json data from database
- (NSString *) getDatabaseResults:(NSString *) api_call_value;

// Pass the value of api call for which you want to access json data
- (NSString *)whereClause:(NSString *) api_call_value;

// Write file name and its NSData content to root directory of App
- (void) writeFileSystem:(NSString* ) fileName :(NSData *) jsonData;

// Fetch data from a file store into root directory of App
- (NSData *) readFileSystem:(NSString* ) fileName :(NSString *) typeExtension;

// This table will delete/truncate data from selected table.
- (void) truncateTable:(NSString *) tableName;

// we are saving images to root directory path
- (void) saveImagesToDirectory:(NSString *) imageURL imageName:(NSString *) imageName;

// Here we are going to fetch images from root path
-(UIImage *) loadSavedImage:(NSString *)fileName;

// Check if the file already exist or not
- (BOOL) isFileExist:(NSString *) fileName;


@end
