//
//  ViewController.m
//  Driver Lala
//
//  Created by Apple on 01/04/15.
//  Copyright (c) 2015 DriverLala. All rights reserved.
//

#import "PickupController.h"

@interface PickupController () {
    
    NSString *globalOrderName;
    NSArray *orders;
    
}

@end

@implementation PickupController

@synthesize tableView, objCustomProtocol;

// pgrm mark ----- -------

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    objCustomProtocol = [[CustomProtocol alloc] init];
    objCustomProtocol.delegate = self;
    
    UINib *nib = [UINib nibWithNibName:@"TableViewCellPickupController" bundle:nil];
    [[self tableView] registerNib:nib forCellReuseIdentifier:@"ItemCell"];
    
    self.navigationController.delegate = self;
    
    self.tableView.rowHeight = 75;
    
    self->orders =  @[@"345645SDFGSF", @"SFDGSF34534",@"yuik445635",@"sSsdfg3453#$%"];
    
    //UIImage *image = [[UIImage imageNamed:@"logout.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    
    //UIBarButtonItem *button = [[UIBarButtonItem alloc] initWithImage:image
                                                               //style:UIBarButtonItemStylePlain target:self action:@selector(logoutApp:)];
    
    //self.tabBarController.navigationItem.rightBarButtonItem = button;


    
}

/**
 * @param id
 *
 * @return void
 *
 * if user wish to logout from the screen
 **/

- (void) logoutApp:(UIStoryboardSegue*)send {
    
    [self.objCustomProtocol setUserDefaults:@"FALSE" key:@"IS_LOGGED_IN"];
    
    [self performSegueWithIdentifier:@"back_to_sign_in_segue" sender:self];
    
}// end logoutApp

// pgrm mark ----- -------

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:true];
    NSLog(@"viewWillAppear PickupController");
    
    [super viewWillAppear:animated];
    self.tabBarController.navigationItem.hidesBackButton=YES;
    
}

// pgrm mark ----- -------

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:true];
    NSLog(@"viewDidAppear PickupController");
    
}


// Number of now in different sections
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return 4;
    
}

// Create a table for different section of app
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    // Create an instance of ItemCell
    TableViewCellPickupController *cell =  [self.tableView dequeueReusableCellWithIdentifier:@"ItemCell"];
    
    
    // show arrow indicator
    cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
    
    cell.orderNameLabel.text = orders[indexPath.row];
    cell.orderPlaceLabel.text = @"Jaipur , Rajasthan";
    
    self->globalOrderName = cell.orderNameLabel.text;
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSLog(@"Method: didSelectRowAtIndexPath");
    
    [self performSegueWithIdentifier:@"order_details_segue" sender:self];
    
    
}// end didSelectRowAtIndexPath

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    
    // Get reference to the destination view controller
    OrderDetailController *vc = [segue destinationViewController];
    
    // Pass any objects to the view controller here, like...
    vc.orderName = self->globalOrderName;
    
}// end prepare for segue

// pgrm mark ----- -------

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end


// Navigation bar color : 254	111	34
// button color reset and next : 253	148	5