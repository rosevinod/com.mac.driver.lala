//
//  PlaceOrderController.h
//  Driver Lala
//
//  Created by Apple on 21/04/15.
//  Copyright (c) 2015 DriverLala. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CustomProtocol.h"

@interface PlaceOrderController : UIViewController < CustomDelegate, UIImagePickerControllerDelegate
, UINavigationControllerDelegate, UITextFieldDelegate >

@property (weak, nonatomic) IBOutlet UITextField *dateForUserDOB;
@property (weak, nonatomic) IBOutlet UIImageView *productImageView;
@property (nonatomic,retain) CustomProtocol *objCustomProtocol;

@end
