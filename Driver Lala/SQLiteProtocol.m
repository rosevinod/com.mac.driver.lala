//
//  SQLiteProtocol.m
//  Spine MD
//
//  Created by arroWebs Inc on 19/10/13.
//  Copyright (c) 2013 arroWebs Inc. All rights reserved.
//

#import "SQLiteProtocol.h"

@implementation SQLiteProtocol

// SQLite Database vars
@synthesize databasePath, myDatabase,statusOfAddingToDB,searchResult;

- (void) prepareJSONDatabase {
    
    // Get the documents directory
    NSArray *dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *docsDir = dirPaths[0];
    
    // Build the path to the database file
    databasePath = [[NSString alloc] initWithString: [docsDir stringByAppendingPathComponent:@"simpleDB.db"]];
    //DLog(@"Database Path: %@", databasePath);
    
    NSFileManager *filemgr = [NSFileManager defaultManager];
    
    if ([filemgr fileExistsAtPath: databasePath ] == NO) {
        
        //DLog(@"In Loop");
        
        const char *dbpath = [databasePath UTF8String];
        
        if (sqlite3_open(dbpath, &myDatabase) == SQLITE_OK) {
            
            char *errMsg;
            
            const char *sql_stmt =
            "CREATE TABLE IF NOT EXISTS simpleDB (ID INTEGER PRIMARY KEY AUTOINCREMENT, api_call TEXT, data_value TEXT)";
            
            if (sqlite3_exec(myDatabase, sql_stmt, NULL, NULL, &errMsg) != SQLITE_OK) {
                statusOfAddingToDB = @"Failed to create table";
                //DLog(@"failed me");
            } else {
                statusOfAddingToDB = @"Success in creating table";
                //DLog(@"pass me");
            }
            
            sqlite3_close(myDatabase);
        } else {
            statusOfAddingToDB = @"Failed to open/create database";
            //DLog(@"Database Status %@",statusOfAddingToDB);
            
        }
    } else {
        statusOfAddingToDB = @"Database already exists";
        //DLog(@"Database Status %@",statusOfAddingToDB);
    }
}// end of method prepareJSONDatabase

// Save different value into database

- (void) saveToDatabase:(NSString *) apiCallName :(NSString *) jsonFileName :(NSData *) jsonData{
    
    //DLog(@"Class: SQLiteProtocol Method: saveToDatabase");
    
    // Converting json NSData to string json i.e ({"success":true,"message":"","total":33,"data":{"doctor_name":"Todd Lanman"..)
    NSString *dataString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    NSLog(@"JSON data needs to be saved %@",dataString);
    
    // An instance of this object represents a single SQL statement
    sqlite3_stmt    *statement;
    
    const char *dbpath = [databasePath UTF8String];
    
    if (sqlite3_open(dbpath, &myDatabase) == SQLITE_OK) {
        
        // Create Query
        NSString *insertSQL = [NSString stringWithFormat:
                               @"INSERT INTO simpleDB (api_call, data_value) VALUES ('%@', '%@')",
                               apiCallName, dataString];
        
        
        // Encode the query string
        const char *insert_stmt = [insertSQL UTF8String];
        
        /* sqlite3_prepare_v2: Database handle */
        /* SQL statement, UTF-8 encoded */
        /* Maximum length of zSql in bytes. */
        /* OUT: Statement handle */
        /* OUT: Pointer to unused portion of zSql */
        
        sqlite3_prepare_v2(myDatabase, insert_stmt,-1, &statement, NULL);
        
        if (sqlite3_step(statement) == SQLITE_DONE) {
            
            statusOfAddingToDB = [NSString stringWithFormat:@"JSON added to Database"];
            
        } else {
            statusOfAddingToDB = @"Failed to add JSON";
        }
        
        //DLog(@"Method: saveToDatabase %@",statusOfAddingToDB);
        
        sqlite3_finalize(statement);
        sqlite3_close(myDatabase);
    }
    
    // Save JSON file into device
    //[self writeFileSystem: jsonFileName : jsonData];
    
}// end of method saveToDatabase


// Save different value into database
- (void) saveImagesToDatabase:(NSString *) apiCallName fileName:(NSString *) fileName{
    
    //DLog(@"Class: SQLiteProtocol Method: saveImagesToDatabase");
    
    // An instance of this object represents a single SQL statement
    sqlite3_stmt    *statement;
    
    const char *dbpath = [databasePath UTF8String];
    
    if (sqlite3_open(dbpath, &myDatabase) == SQLITE_OK) {
        
        // Create Query
        NSString *insertSQL = [NSString stringWithFormat:
                               @"INSERT INTO simpleDB (api_call, data_value) VALUES ('%@', '%@')",
                               apiCallName, fileName];
        
        
        // Encode the query string
        const char *insert_stmt = [insertSQL UTF8String];
        
        /* sqlite3_prepare_v2: Database handle */
        /* SQL statement, UTF-8 encoded */
        /* Maximum length of zSql in bytes. */
        /* OUT: Statement handle */
        /* OUT: Pointer to unused portion of zSql */
        
        sqlite3_prepare_v2(myDatabase, insert_stmt,-1, &statement, NULL);
        
        if (sqlite3_step(statement) == SQLITE_DONE) {
            
            statusOfAddingToDB = [NSString stringWithFormat:@"Images added to Database"];
            
        } else {
            statusOfAddingToDB = @"Failed to add images";
        }
        
        //DLog(@"Message: saveImagesToDatabase %@",statusOfAddingToDB);
        
        sqlite3_finalize(statement);
        sqlite3_close(myDatabase);
    }
    
    // Save JSON file into device
    [self saveImagesToDirectory:apiCallName imageName:fileName];
    
}// end of method saveImagesToDatabase

// we are saving images to root directory path

- (void) saveImagesToDirectory:(NSString *) imageURL imageName:(NSString *) imageName {
    
    //DLog(@"Class: SQLiteProtocol Method: saveImagesToDirectory");
    
    //DLog(@"Downloading images....");
    
	// Get an image from the URL below
	UIImage *image = [[UIImage alloc] initWithData:[NSData
                                                    dataWithContentsOfURL:[NSURL URLWithString:imageURL]]];
    
    //DLog(@"Image URL %@",imageURL);
    
	//DLog(@"Width Of Image: %f, Height Of Image: %f",image.size.width,image.size.height);
    
	// Let's save the file into Document folder.
	NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
	// If you go to the folder below, you will find those pictures
	//DLog(@"%@",docDir);
    
	//DLog(@"Saving png....");
	NSString *pngFilePath = [NSString stringWithFormat:@"%@/%@",docDir,imageName];
	NSData *dataPNG = [NSData dataWithData:UIImagePNGRepresentation(image)];
	[dataPNG writeToFile:pngFilePath atomically:YES];
    
	//DLog(@"Saving jpeg if any*....");
	NSString *jpegFilePath = [NSString stringWithFormat:@"%@/%@",docDir,imageName];
	NSData *dataJPG = [NSData dataWithData:UIImageJPEGRepresentation(image, 1.0f)];//1.0f = 100% quality
	[dataJPG writeToFile:jpegFilePath atomically:YES];
    
	//DLog(@"Saving image done");
    
    
}// End of the method saveImagesToDirectory

// Here we are going to fetch images from root path

- (UIImage *) loadSavedImage:(NSString *)fileName {
    
    //DLog(@"Class: SQLiteProtocol Method: loadSavedImage");
    
    // Let's save the file into Document folder.
	NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    NSString *path = [NSString stringWithFormat:@"%@/%@",docDir,fileName];
    
    //DLog(@"Path where image exists %@" ,path);
    
    UIImage * result = [UIImage imageWithContentsOfFile:path];
    
    return result;
}// end of method loadImage


// Get the value of json, will send an api call for which we get json data from database
- (NSString *) getDatabaseResults:(NSString *) apiCallName {
    
    //DLog(@"Class: SQLiteProtocol Method: getDatabaseResults");
    
    NSMutableArray *store = [[NSMutableArray alloc] init];
    
    NSString *docsDir;
    NSArray *dirPaths;
    
    NSString *whereClauseStr = [self whereClause:apiCallName];
    
    // Get the documents directory
    dirPaths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    docsDir = dirPaths[0];
    
    // Build the path to the database file
    databasePath = [[NSString alloc]
                    initWithString: [docsDir stringByAppendingPathComponent:
                                     @"simpleDB.db"]];
    
    const char *dbpath = [databasePath UTF8String];
    sqlite3_stmt    *statement;
    
    if (sqlite3_open(dbpath, &myDatabase) == SQLITE_OK)
    {
        NSString *querySQL = [NSString stringWithFormat: @"SELECT * FROM simpleDB WHERE %@", whereClauseStr];
        //NSString *querySQL = [NSString stringWithFormat: @"SELECT * FROM simpleDB"];
        
        const char *query_stmt = [querySQL UTF8String];
        
        if (sqlite3_prepare_v2(myDatabase, query_stmt, -1, &statement, NULL) == SQLITE_OK) {
            
            [store removeAllObjects];
            while (sqlite3_step(statement) == SQLITE_ROW) {
                
                //NSString *pID = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 0)];
                //NSString *api_call = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 1)];
                NSString *dataValue = [[NSString alloc] initWithUTF8String: (const char *) sqlite3_column_text(statement, 2)];
                
                //DLog(@"row value %@",dataValue);
                
                [store addObject:[NSString stringWithFormat:@"%@", dataValue]];
                
                //DLog(@"Result Array: In while %@",store);
            }
            
            sqlite3_finalize(statement);
            
        } else {
            //DLog(@"No Result for searched query");
        }
        
        sqlite3_close(myDatabase);
    }
    
    //For Checking
    for (int i = 0; i < [store count]; i++) {
        //DLog(@"Result: %@", [store objectAtIndex:i]);
    }
    
    // Return first element of array
    return [store objectAtIndex:0];
    
} // End of methods getDatabaseResults

// Pass the value of api call for which you want to access json data

- (NSString *)whereClause:(NSString *) api_call_value {
    
    NSString *where = @"";
    BOOL withWhereClause = NO;
    
    if (api_call_value.length > 0) {
        where = [where stringByAppendingString:[NSString stringWithFormat:@"api_call=\"%@\"", api_call_value]];
        withWhereClause = YES;
    }
    
    //DLog(@"where clause: %@", where);
    return where;
}

// This method will write JSON data to on file system.
// @IMPORTANT: Please be careful while edit this function it could effect many files

- (void) writeFileSystem:(NSString* ) fileName :(NSData *) jsonData {
    
    //DLog(@"Class: SQLiteProtocol Method: writeFileSystem");
    
    // Path of root directory
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    // Get dictory files
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    // the path to write file
    NSString *myFile = [documentsDirectory stringByAppendingPathComponent:fileName];
    
    [jsonData writeToFile:myFile atomically:YES];
    
    /*File Path could be like this: /Users/Username/Library/Application Support/iPhone Simulator/7.0/Applications/A0311CEB-8A2A-4D67-AE33-D367F97350DF/Documents/login_auth.json*/
    
}// End of method,

// This method will write JSON data to on file system.

- (NSData *) readFileSystem:(NSString* ) fileName :(NSString *) typeExtension {
    
    //DLog(@"Class: SQLiteProtocol Method: readFileSystem");
    
    // Path of root directory
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    
    // Get dictory files
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    // The path to write file
    NSString *rootFileName = [documentsDirectory stringByAppendingPathComponent:fileName];
    
    // Get data into NSData files
    NSData *myData = [NSData dataWithContentsOfFile:rootFileName];
    
    return myData;
    
}// End of method,


// This table will delete/truncate data from selected table.

- (void) truncateTable:(NSString *) tableName {
    
    // An instance of this object represents a single SQL statement
    sqlite3_stmt    *statement;
    
    const char *dbpath = [databasePath UTF8String];
    
    if (sqlite3_open(dbpath, &myDatabase) == SQLITE_OK) {
        
        NSString *sqlstatemtn = [NSString stringWithFormat: @"delete from %@",tableName];
        
        const char *del_stmt = [sqlstatemtn UTF8String];
        
        sqlite3_prepare_v2(myDatabase, del_stmt,-1, &statement, NULL);
        
        if (sqlite3_step(statement) == SQLITE_DONE) {
            //DLog(@"Datbase table %@ has been empty now",tableName);
        } else {
            //DLog(@"Datbase table not updated/Empty to overwrite");
        }
        sqlite3_finalize(statement);
        sqlite3_close(myDatabase);
        
    }
    
}// end of truncateTable

// Check is file exist in system or not
- (BOOL) isFileExist:(NSString *) fileName {
    
    // Let's save the file into Document folder.
	NSString *docDir = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
    
    NSString* filePath = [docDir stringByAppendingPathComponent:fileName];
    
    BOOL fileExists = [[NSFileManager defaultManager] fileExistsAtPath:filePath];
    
    return fileExists;
}

@end
