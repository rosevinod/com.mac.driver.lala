//
//  RecordController.m
//  Driver Lala
//
//  Created by Apple on 04/05/15.
//  Copyright (c) 2015 DriverLala. All rights reserved.
//

#import "RecordController.h"

@interface RecordController ()

@end

@implementation RecordController

@synthesize objSQLiteProtocol, objCustomProtocol;

// pgrm mark ----- -------

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    NSLog(@"viewDidLoad %@",NSStringFromClass([self class]));
    
    self.objCustomProtocol = [[CustomProtocol alloc] init];
    self.objCustomProtocol.delegate = self;
    
    // Prepare delegate for the object, Now we can access delegate methods.
    objSQLiteProtocol = [[SQLiteProtocol alloc] init];
    objSQLiteProtocol.delegate = self;

    // set the database ready for the system
    [objSQLiteProtocol prepareJSONDatabase];
    
    // Delete previous data before loading new/fresh data
    [objSQLiteProtocol truncateTable:@"simpleDB"];
  
    
}

// pgrm mark ----- -------

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:true];
    
    NSLog(@"viewWillAppear %@",NSStringFromClass([self class]));
    
    NSString *url = [objCustomProtocol getPlistVars:@"login_url"];
    
    // Fetch json data from URL
    NSData * jsonNSData = [objCustomProtocol JSONHandler:url];
    
    // Save file and its related data
    [objSQLiteProtocol saveToDatabase: @"login_url" : @"login_url" : jsonNSData];
    
    // Get result into string format
    NSString *getNSStringDocRes = [objSQLiteProtocol getDatabaseResults:@"login_url"];
    
    NSLog(@"Result from database %@",getNSStringDocRes);
}

// pgrm mark ----- -------

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:true];
    NSLog(@"viewDidAppear %@",NSStringFromClass([self class]));
    
}

// pgrm mark ----- -------

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end