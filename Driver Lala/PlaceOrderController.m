//
//  PlaceOrderController.m
//  Driver Lala
//
//  Created by Apple on 21/04/15.
//  Copyright (c) 2015 DriverLala. All rights reserved.
//

#import "PlaceOrderController.h"

@interface PlaceOrderController ()

@end

@implementation PlaceOrderController

@synthesize dateForUserDOB, productImageView, objCustomProtocol;

// pgrm mark ----- -------

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    NSLog(@"viewDidLoad %@",NSStringFromClass([self class]));
    
    self.dateForUserDOB.delegate = self;
    
    self.objCustomProtocol = [[CustomProtocol alloc] init];
    self.objCustomProtocol.delegate = self;
    
    UIToolbar *toolbar = [[UIToolbar alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    //toolbar.barStyle   = UIBarStyleBlackTranslucent;
    
    UIBarButtonItem *itemDone  = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemDone
                                                                               target:dateForUserDOB action:@selector(resignFirstResponder)];
    
    UIBarButtonItem *itemSpace = [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace
                                                                               target:nil action:nil];
    
    toolbar.items = @[itemSpace,itemDone];
    
    
    UIDatePicker *datePicker = [[UIDatePicker alloc]init];
    [datePicker setDate:[NSDate date]];
    datePicker.datePickerMode = UIDatePickerModeDate;
    [datePicker addTarget:self action:@selector(dateTextField:) forControlEvents:UIControlEventValueChanged];
    
    [dateForUserDOB setInputView:datePicker];
    [dateForUserDOB setInputAccessoryView:toolbar];
    
    [dateForUserDOB becomeFirstResponder];
    
    // check if the device don't have any camera
    if (![UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
        
        [[self objCustomProtocol] showAlertMessage:@"Device has no camera"];
        
    }// end if
    
    
}

-(void) dateTextField:(id)sender {
    
    
    UIDatePicker *picker = (UIDatePicker*)dateForUserDOB.inputView;
    [picker setMaximumDate:[NSDate date]];
    NSDateFormatter *dateFormat = [[NSDateFormatter alloc] init];
    NSDate *eventDate = picker.date;
    [dateFormat setDateFormat:@"dd/MM/yyyy"];
    
    NSString *dateString = [dateFormat stringFromDate:eventDate];
    dateForUserDOB.text = [NSString stringWithFormat:@"%@",dateString];
}


// pgrm mark ----- -------

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:true];
    
    NSLog(@"viewWillAppear %@",NSStringFromClass([self class]));
    
}

// pgrm mark ----- -------

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:true];
    NSLog(@"viewDidAppear %@",NSStringFromClass([self class]));
    
}

- (IBAction)dobTextEditAction:(UITextField *)sender {
    
    
    
}// end dobTextEditAction


- (IBAction)takePhotoButton:(UIButton *)sender {

    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypeCamera;
    
    [self presentViewController:picker animated:YES completion:NULL];
    
    
}// end takePhotoButton

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    self.productImageView.image = chosenImage;
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
    
}

// pgrm mark ----- -------

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end

