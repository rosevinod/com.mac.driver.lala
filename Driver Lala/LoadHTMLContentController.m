//
//  LoadHTMLContentController.m
//  Driver Lala
//
//  Created by Apple on 20/04/15.
//  Copyright (c) 2015 DriverLala. All rights reserved.
//

#import "LoadHTMLContentController.h"

@interface LoadHTMLContentController ()

@end

@implementation LoadHTMLContentController

@synthesize webView;

// pgrm mark ----- -------

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    NSLog(@"viewDidLoad %@",NSStringFromClass([self class]));
    
    NSURL *url = [NSURL URLWithString:@"http://babs-development-env-ermzyyqtet.elasticbeanstalk.com/index.php/mobApi/StaticPage/page/terms_conditions"];
    NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
    [self.webView loadRequest:requestObj];
    self.webView.scalesPageToFit = YES;
    
}

// pgrm mark ----- -------

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:true];
    
    NSLog(@"viewWillAppear %@",NSStringFromClass([self class]));
    
}

// pgrm mark ----- -------

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:true];
    NSLog(@"viewDidAppear %@",NSStringFromClass([self class]));
    
}

// pgrm mark ----- -------

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)closeBarButtonAction:(UIBarButtonItem *)sender {
    
    [self dismissViewControllerAnimated:true completion:nil];
    
}// end closeBarButtonAction

- (IBAction)goToBackBarButtonAction:(UIBarButtonItem *)sender {
    
}// end goToBackBarButtonAction

- (IBAction)goToForwardBarButtonAction:(UIBarButtonItem *)sender {
    
}


@end
