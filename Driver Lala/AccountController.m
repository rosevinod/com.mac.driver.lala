//
//  AccountController.m
//  Driver Lala
//
//  Created by Apple on 14/04/15.
//  Copyright (c) 2015 DriverLala. All rights reserved.
//

#import "AccountController.h"

@interface AccountController () {
    
    NSDictionary *animals;
    NSArray *animalSectionTitles;
    
}

@end

@implementation AccountController

@synthesize tableView;

// pgrm mark ----- -------

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    animals = @{@"My Fans" : @[@"My Fans", @"Tutorial"],
                @"Account" : @[@"Profile", @"Stats", @"Ratings"],
                @"Information" : @[@"News", @"Etiquette",@"Rates",@"FAQ"],
                @"Share" : @[@"Share"],
                @"Settings" : @[@"Language", @"Vibration notification", @"Sound notification",@"Location"],
                @"EasyVan" : @[@"Opinion", @"About us",@"Tutorial"]};
    
    animalSectionTitles = [[animals allKeys] sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
    
    
}

// pgrm mark ----- -------

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:true];
    NSLog(@"hello i am viewWillAppear");
    
}

// pgrm mark ----- -------

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:true];
    NSLog(@"hello i am viewDidAppear");
    
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    // Return the number of sections.
    return [animalSectionTitles count];
}


// Number of now in different sections
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    // Return the number of rows in the section.
    NSString *sectionTitle = [animalSectionTitles objectAtIndex:section];
    NSArray *sectionAnimals = [animals objectForKey:sectionTitle];
    return [sectionAnimals count];
    
}

// Create a table for different section of app
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    static NSString *cellIdentifier = @"YoYo";
    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:cellIdentifier];
    
    if (!cell) {
        
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleSubtitle
                                      reuseIdentifier:cellIdentifier];
        // show arrow indicator
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        
    }
    
    // Configure the cell...
    NSString *sectionTitle = [animalSectionTitles objectAtIndex:indexPath.section];
    NSArray *sectionAnimals = [animals objectForKey:sectionTitle];
    NSString *animal = [sectionAnimals objectAtIndex:indexPath.row];
    cell.imageView.image = [UIImage imageNamed:animal];
    cell.textLabel.text = animal;
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSLog(@"Method: didSelectRowAtIndexPath");
    
    LoadHTMLContentController *webViewLoaderViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"LoadHTMLContentController"];
    
    [self presentViewController:webViewLoaderViewController animated:YES completion:nil];
    
    
}// end didSelectRowAtIndexPath

- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    return [animalSectionTitles objectAtIndex:section];
}

// pgrm mark ----- -------

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end