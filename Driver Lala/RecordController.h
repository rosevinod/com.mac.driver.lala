//
//  RecordController.h
//  Driver Lala
//
//  Created by Apple on 04/05/15.
//  Copyright (c) 2015 DriverLala. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SQLiteProtocol.h"
#import "CustomProtocol.h"

@interface RecordController : UIViewController < CustomDelegate, SQLiteProtocolDelegate >

@property (nonatomic,retain) SQLiteProtocol * objSQLiteProtocol;
@property (nonatomic,retain) CustomProtocol *objCustomProtocol;

@end
