//
//  HomeTabViewController.m
//  Driver Lala
//
//  Created by Apple on 29/04/15.
//  Copyright (c) 2015 DriverLala. All rights reserved.
//

#import "HomeTabViewController.h"

@interface HomeTabViewController ()

@end

@implementation HomeTabViewController
@synthesize objCustomProtocol;

// pgrm mark ----- -------

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    NSLog(@"viewDidLoad %@",NSStringFromClass([self class]));
    
    objCustomProtocol = [[CustomProtocol alloc] init];
    objCustomProtocol.delegate = self;
    
    
}


// pgrm mark ----- -------

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:true];
    
    NSLog(@"viewWillAppear %@",NSStringFromClass([self class]));
    
}

// pgrm mark ----- -------

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:true];
    NSLog(@"viewDidAppear %@",NSStringFromClass([self class]));
    
}

- (IBAction)logoutBarButton:(UIBarButtonItem *)sender {
    
    [self.objCustomProtocol setUserDefaults:@"FALSE" key:@"IS_LOGGED_IN"];
    
    // unwind or back to any view controller
    [self performSegueWithIdentifier:@"hom_logout_segue" sender:self];
    
}// end logoutBarButton


// pgrm mark ----- -------

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
