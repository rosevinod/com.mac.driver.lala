//
//  LoadHTMLContentController.h
//  Driver Lala
//
//  Created by Apple on 20/04/15.
//  Copyright (c) 2015 DriverLala. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoadHTMLContentController : UIViewController

@property (weak, nonatomic) IBOutlet UIWebView *webView;

@end
