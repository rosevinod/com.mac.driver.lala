//
//  PickupController.h
//  Driver Lala
//
//  Created by Apple on 01/04/15.
//  Copyright (c) 2015 DriverLala. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TableViewCellPickupController.h"
#import "OrderDetailController.h"
#import "CustomProtocol.h"

@interface PickupController : UIViewController < CustomDelegate, UINavigationControllerDelegate
,  UITableViewDataSource, UITableViewDelegate >

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@property (nonatomic,retain) CustomProtocol *objCustomProtocol;

@end

