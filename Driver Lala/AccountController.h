//
//  AccountController.h
//  Driver Lala
//
//  Created by Apple on 14/04/15.
//  Copyright (c) 2015 DriverLala. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoadHTMLContentController.h"

@interface AccountController : UIViewController < UITableViewDataSource, UITableViewDelegate >

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end
