//
//  SpineCustomProtocol.m
//  Spine MD
//
//  Created by arroWebs Inc on 17/10/13.
//  Copyright (c) 2013 arroWebs Inc. All rights reserved.
//

#import "CustomProtocol.h"

@implementation CustomProtocol

// Set Global values for the Application

- (void) setUserDefaults:(NSString *) setObject key:(NSString *) forKey{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:setObject forKey:forKey];
    [defaults synchronize];
    
}// end of Method setUserDefaults

// Get Global Values from Application

- (NSString *) getUserDefaults:(NSString *) objectForKey{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *resultObject = [defaults objectForKey:objectForKey];
    
    return resultObject;
}// End of Method getUserDefaults

// This method will check authentication of user/patients

- (NSString *) checkAuthentication:(NSString * ) phoneNumber : (NSString *) password {
    
    NSLog(@"Class:CustomProtocal Method:checkAuthentication");
    
    NSString *siteURLstr = [self getPlistVars:@"login_url"];
    
    //[NSString stringWithFormat:@"http://192.168.1.14/webclient/static_api.php?action=login"];
   
    // Get result into parameters from dispatch_async
    NSData *jsonResult;
    
    // Get data from another thread
    jsonResult = [self JSONHandler:siteURLstr];
    
    // Send jsonResult to another class
    [self.delegate authenticationResults:jsonResult];
    
    return nil;
}// end of method checkAuthentication

// This method will handle all type of JSON Parameters will return NSData value

- (NSData * ) JSONHandler:(NSString *)siteURL {
    
    @try {
        
        NSURL *url=[NSURL URLWithString:siteURL];
        
        NSData *data=[NSData dataWithContentsOfURL:url];
        
        NSLog(@"JSON Data has been executued successfully");
        
        return data;
        
    } @catch(NSException *theException) {
        
        NSLog(@"An exception occurred: %@", theException.name);
        NSLog(@"Here are some details: %@", theException.reason);
        
    } @finally {
        NSLog(@"Class:SpineCustomProtocal Method:JSONHandler Executing finally block");
    }
    
    
}// end of method JSONHandler

// This method will give parameter value for plist

- (NSString *) getPlistVars:(NSString *) keyName {
    
    NSString *path = [[NSBundle mainBundle] pathForResource:@"ServerUri" ofType:@"plist"];
    
    NSDictionary *dict = [[NSDictionary alloc] initWithContentsOfFile:path];
    
    NSString *spine_var = [dict objectForKey:keyName] ;
    
    NSLog(@"Method: getPlistVars %@",spine_var);
    
    return spine_var;
    
}// end of method getPlistVars

// Check is network available in iPhone/iPad

- (NetworkStatus) checkNetworkAvailability {
    
    Reachability* wifiReach = [Reachability reachabilityWithHostName: @"www.apple.com"];
    NetworkStatus remoteHostStatus = [wifiReach currentReachabilityStatus];
    
    switch (remoteHostStatus) {
        case NotReachable: {
            NSLog(@"Access Not Available");
            break;
        }
        case ReachableViaWWAN: {
            NSLog(@"Reachable WWAN");
            break;
        }
        case ReachableViaWiFi: {
            NSLog(@"Reachable WiFi");
            break;
        }
    }
    return remoteHostStatus;
    
}// end checkNetworkAvailability

// Show error alert as
- (void) showAlertMessage:(NSString *) message {
    
    UIAlertView *ErrorAlert = [[UIAlertView alloc] initWithTitle:@"" message:message delegate:nil
                                               cancelButtonTitle:@"OK"
                                               otherButtonTitles:nil, nil];
    [ErrorAlert show];
    
}// end of method showErrorAlert

- (void) activityIndicator:(UIView *)view container:(UIView *)container
               loadingView:(UIView *)loadingView
                    actInd:(UIActivityIndicatorView *)actInd status:(BOOL)status {
    
    // this will be shown in status bar
    [UIApplication sharedApplication].networkActivityIndicatorVisible = status;
    
    //UIView *container = [[UIView alloc] init];
    //UIView *loadingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 80, 80)];
    //UIActivityIndicatorView *actInd = [[UIActivityIndicatorView alloc] init];
    container.frame = view.frame;
    container.center = view.center;
    container.backgroundColor = [UIColor colorWithRed:255.0f/255.0f
                                                        green:255.0f/255.0f
                                                         blue:255.0f/255.0f
                                                        alpha:0.3f];
    
    // create a square in which indicator will be displayed
    //UIView *loadingView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 80, 80)];
    loadingView.center = view.center;
    loadingView.backgroundColor = [UIColor colorWithRed:68.0f/255.0f
                                                  green:68.0f/255.0f
                                                   blue:68.0f/255.0f
                                                  alpha:0.7f];
    
    // this indicator will be show in self.view view controller
    //UIActivityIndicatorView *actInd = [[UIActivityIndicatorView alloc] init];
    
    actInd.frame = CGRectMake(0.0, 0.0, 40.0, 40.0);
    actInd.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhiteLarge;
    actInd.center = CGPointMake(loadingView.frame.size.width / 2,
                                loadingView.frame.size.height / 2);
    

    loadingView.clipsToBounds = true;
    loadingView.layer.cornerRadius = 10;

    [loadingView addSubview:actInd];
    [container addSubview:loadingView];
    [view addSubview: container];
    
    if(status) {
        [actInd startAnimating];
    } else {
        NSLog(@"stop animationg");
        [container removeFromSuperview];
        [actInd stopAnimating];
    }
    
    
}// end showActivityIndicator



@end
