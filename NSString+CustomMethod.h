//
//  NSString+CustomMethod.h
//  Driver Lala
//
//  Created by Apple on 29/04/15.
//  Copyright (c) 2015 DriverLala. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CommonCrypto/CommonDigest.h>


@interface NSString (CustomMethod)

+ (NSString*)MD5:(NSString *)string;

@end
